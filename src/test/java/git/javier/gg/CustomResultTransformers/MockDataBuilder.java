package git.javier.gg.CustomResultTransformers;

public class MockDataBuilder {

	TupleAlias inputTuple = new TupleAlias();

	public MockDataBuilder() {
	}

	public void addAliases(String aliases, Object value) {
		inputTuple.getAlias().add(aliases);
		inputTuple.getTuple().add(value);

	}

	public String[] getAliases() {
		return inputTuple.getAliasAsArray();
	}

	public Object[] getTuple() {
		return inputTuple.getTupleAsArray();
	}

}
