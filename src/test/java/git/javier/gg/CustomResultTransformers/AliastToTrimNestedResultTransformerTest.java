package git.javier.gg.CustomResultTransformers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.hibernate.HibernateException;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.ResultTransformer;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.OrderWith;
import org.junit.runner.manipulation.Alphanumeric;

import git.javier.gg.testObject.Persona;
import git.javier.gg.testObject.PersonaFactory;
import git.javier.gg.testObject.isSon;

/**
 * The basic test cases for the {@link AliastToFullNestedResultTransformer}
 * 
 * @author Javier Gonzalez
 * @version 1.0
 * @since 25 jul. 2020
 */
@OrderWith(value = Alphanumeric.class)
public class AliastToTrimNestedResultTransformerTest {

	private static ResultTransformer nestedTransformer;
	private static ResultTransformer singleTransformer;

	@Before
	public void beforeAll() {

		singleTransformer = new AliasToBeanResultTransformer(Persona.class);
		nestedTransformer = new AliastToTrimNestedResultTransformer(Persona.class);
	}

	@Test
	public void transforms_withNullListProperty_areEquals() {
		Persona personaWithParents = PersonaFactory.getPersonaWithParentsShallow();
		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("nombre", personaWithParents.getNombre());
		data.addAliases("hijos", personaWithParents.getHijos());

		Persona resultSingle = (Persona) singleTransformer.transformTuple(data.getTuple(), data.getAliases());
		Persona resultNested = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertEquals(resultNested, resultSingle);
	}

	@Test
	public void transforms_withEmptyData_areEquals() {

		isSon resultSingle = (isSon) singleTransformer.transformTuple(new Object[] {}, new String[] {});
		isSon resultNested = (isSon) nestedTransformer.transformTuple(new Object[] {}, new String[] {});

		assertEquals(resultSingle, resultNested);
	}

	@Test
	public void transforms_withShallowProperties_areEquals() {
		Persona personaWithParents = PersonaFactory.getPersonaWithParentsShallow();
		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("nombre", personaWithParents.getNombre());
		data.addAliases("edad", personaWithParents.getEdad());

		Persona resultSingle = (Persona) singleTransformer.transformTuple(data.getTuple(), data.getAliases());
		Persona resultNested = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertEquals(resultSingle, resultNested);

		assertNotNull(resultSingle.getNombre());
		assertNotNull(resultSingle.getEdad());
	}

	@Test(expected = HibernateException.class)
	public void transformSingle_withOneLevelNested_notSupported() {
		Persona personaWithParents = PersonaFactory.getPersonaWithParentsShallow();
		MockDataBuilder data = new MockDataBuilder();

		data.addAliases("nombre", personaWithParents.getNombre());
		data.addAliases("edad", personaWithParents.getEdad());
		data.addAliases("madre.nombre", personaWithParents.getMadre().getNombre());
		data.addAliases("madre.edad", personaWithParents.getMadre().getEdad());
		data.addAliases("padre.estatura", personaWithParents.getPadre().getEstatura());

		Persona resultSingle = (Persona) singleTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(resultSingle);
		assertNotNull(resultSingle.getNombre());
		assertNotNull(resultSingle.getEdad());
		assertNotNull(resultSingle.getMadre().getEdad());
		assertNotNull(resultSingle.getMadre().getNombre());
		assertNotNull(resultSingle.getPadre().getEstatura());
	}

	@Test()
	public void transformNested_withOneLevelNested() {
		Persona personaWithParents = PersonaFactory.getPersonaWithParentsShallow();
		MockDataBuilder data = new MockDataBuilder();

		data.addAliases("nombre", personaWithParents.getNombre());
		data.addAliases("edad", personaWithParents.getEdad());
		data.addAliases("madre.nombre", personaWithParents.getMadre().getNombre());
		data.addAliases("madre.edad", personaWithParents.getMadre().getEdad());
		data.addAliases("padre.estatura", personaWithParents.getPadre().getEstatura());

		Persona resultSingle = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(resultSingle);
		assertNotNull(resultSingle.getNombre());
		assertNotNull(resultSingle.getEdad());
		assertNotNull(resultSingle.getMadre().getEdad());
		assertNotNull(resultSingle.getMadre().getNombre());
		assertNotNull(resultSingle.getPadre().getEstatura());
	}

	@Test(expected = HibernateException.class)
	public void transformSingle_withTwoLevelNested_notSupported() {
		Persona personaWithGrandparents = PersonaFactory.getPersonaWithGrandParentsShallow();
		MockDataBuilder data = new MockDataBuilder();

		data.addAliases("nombre", personaWithGrandparents.getNombre());
		data.addAliases("edad", personaWithGrandparents.getEdad());
		data.addAliases("madre.nombre", personaWithGrandparents.getMadre().getNombre());
		data.addAliases("madre.edad", personaWithGrandparents.getMadre().getEdad());
		data.addAliases("madre.madre.nombre", personaWithGrandparents.getMadre().getMadre().getNombre());
		data.addAliases("madre.madre.edad", personaWithGrandparents.getMadre().getMadre().getEdad());

		Persona result = (Persona) singleTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getEdad());
		assertNotNull(result.getMadre().getEdad());
		assertNotNull(result.getMadre().getNombre());
		assertNotNull(result.getMadre().getMadre().getEdad());
		assertNotNull(result.getMadre().getMadre().getNombre());
	}

	@Test()
	public void transformNested_withTwoLevelNested() {
		Persona personaWithGrandparents = PersonaFactory.getPersonaWithGrandParentsShallow();
		MockDataBuilder data = new MockDataBuilder();

		data.addAliases("nombre", personaWithGrandparents.getNombre());
		data.addAliases("edad", personaWithGrandparents.getEdad());
		data.addAliases("madre.nombre", personaWithGrandparents.getMadre().getNombre());
		data.addAliases("madre.edad", personaWithGrandparents.getMadre().getEdad());
		data.addAliases("madre.madre.nombre", personaWithGrandparents.getMadre().getMadre().getNombre());
		data.addAliases("madre.madre.edad", personaWithGrandparents.getMadre().getMadre().getEdad());

		Persona result = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getEdad());
		assertNotNull(result.getMadre().getEdad());
		assertNotNull(result.getMadre().getNombre());
		assertNotNull(result.getMadre().getMadre().getEdad());
		assertNotNull(result.getMadre().getMadre().getNombre());
	}

	@Test(expected = HibernateException.class)
	public void transformSingle_withTwoLevelNestedUnordened_notSopported() {
		Persona personaWithGrandparents = PersonaFactory.getPersonaWithGrandParentsShallow();
		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("madre.madre.edad", personaWithGrandparents.getMadre().getMadre().getEdad());
		data.addAliases("madre.nombre", personaWithGrandparents.getMadre().getNombre());
		data.addAliases("padre.padre.edad", personaWithGrandparents.getPadre().getPadre().getEdad());
		data.addAliases("edad", personaWithGrandparents.getEdad());
		data.addAliases("madre.madre.nombre", personaWithGrandparents.getMadre().getMadre().getNombre());
		data.addAliases("nombre", personaWithGrandparents.getNombre());
		data.addAliases("madre.edad", personaWithGrandparents.getMadre().getEdad());

		Persona result = (Persona) singleTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getEdad());
		assertNotNull(result.getMadre().getEdad());
		assertNotNull(result.getMadre().getNombre());
		assertNotNull(result.getMadre().getMadre().getEdad());
		assertNotNull(result.getMadre().getMadre().getNombre());
		assertNotNull(result.getPadre().getPadre().getEdad());
	}

	@Test
	public void transformNested_withTwoLevelNestedUnordened() {
		Persona personaWithGrandparents = PersonaFactory.getPersonaWithGrandParentsShallow();
		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("madre.madre.edad", personaWithGrandparents.getMadre().getMadre().getEdad());
		data.addAliases("madre.nombre", personaWithGrandparents.getMadre().getNombre());
		data.addAliases("padre.padre.edad", personaWithGrandparents.getPadre().getPadre().getEdad());
		data.addAliases("edad", personaWithGrandparents.getEdad());
		data.addAliases("madre.madre.nombre", personaWithGrandparents.getMadre().getMadre().getNombre());
		data.addAliases("nombre", personaWithGrandparents.getNombre());
		data.addAliases("madre.edad", personaWithGrandparents.getMadre().getEdad());

		Persona result = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getEdad());
		assertNotNull(result.getMadre().getEdad());
		assertNotNull(result.getMadre().getNombre());
		assertNotNull(result.getMadre().getMadre().getEdad());
		assertNotNull(result.getMadre().getMadre().getNombre());
		assertNotNull(result.getPadre().getPadre().getEdad());
	}

	@Test(expected = HibernateException.class)
	public void transformSingle_withTwoLevelWithNull_notSupportedt() {
		Persona personaWithGrandparents = PersonaFactory.getPersonaWithGrandParentsShallow();
		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("nombre", personaWithGrandparents.getNombre());
		data.addAliases("edad", personaWithGrandparents.getEdad());
		data.addAliases("madre.nombre", personaWithGrandparents.getMadre().getNombre());
		data.addAliases("madre.edad", personaWithGrandparents.getMadre().getEdad());
		data.addAliases("madre.madre.nombre", null);
		data.addAliases("madre.madre.edad", null);

		Persona result = (Persona) singleTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getEdad());
		assertNotNull(result.getMadre().getEdad());
		assertNotNull(result.getMadre().getNombre());
		assertNull(result.getMadre().getMadre().getEdad());
		assertNull(result.getMadre().getMadre().getNombre());
	}

	@Test()
	public void transformNested_withTwoLevelWithNull() {
		Persona personaWithGrandparents = PersonaFactory.getPersonaWithGrandParentsShallow();
		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("nombre", personaWithGrandparents.getNombre());
		data.addAliases("edad", personaWithGrandparents.getEdad());
		data.addAliases("madre.nombre", personaWithGrandparents.getMadre().getNombre());
		data.addAliases("madre.edad", personaWithGrandparents.getMadre().getEdad());
		data.addAliases("madre.madre.nombre", null);
		data.addAliases("madre.madre.edad", null);
		data.addAliases("unknow.bean", null); // path aliases will be ignored because result-value is null

		Persona result = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getEdad());
		assertNotNull(result.getMadre().getEdad());
		assertNotNull(result.getMadre().getNombre());
		assertNull(result.getMadre().getMadre());
	}

	@Test
	public void transformNested_withValidListProperty_alreadyBuildList() {
		Persona personaWithParents = PersonaFactory.getPersonaWithGrandParentsShallow();
		Persona personaWithSons = personaWithParents.getMadre();

		MockDataBuilder data = new MockDataBuilder();

		data.addAliases("nombre", personaWithSons.getNombre());
		data.addAliases(null, null);
		data.addAliases("hijos", personaWithSons.getHijos());

		Persona result = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getHijos());
		assertFalse(result.getHijos().isEmpty());
	}

	@Test
	@Ignore(value = "Not represent a valid tuple generate from hibernate. requiered more invest")
	public void transformNested_withNestedValidListProperty_alreadyBuildList() {
		Persona personaWithParents = PersonaFactory.getPersonaWithGrandParentsShallow();
		Persona personaWithSons = personaWithParents.getMadre();
		MockDataBuilder data = new MockDataBuilder();

		data.addAliases("nombre", personaWithSons.getNombre());
		data.addAliases("madre.nombre", personaWithSons.getMadre().getNombre());
		data.addAliases("madre.hijos", personaWithSons.getMadre().getHijos());

		Persona result = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getMadre().getHijos());
		assertNotNull(result.getMadre().getNombre());
	}

	@Test
	@Ignore(value = "Not represent a valid tuple generate from hibernate. requiered more invest")
	public void transformNested_withValidListProperty_notBuild() {
		Persona personaWithParents = PersonaFactory.getPersonaWithGrandParentsShallow();
		Persona personaWithSons = personaWithParents.getMadre();
		MockDataBuilder data = new MockDataBuilder();

		data.addAliases("nombre", personaWithSons.getNombre());
		data.addAliases("madre.nombre", personaWithSons.getMadre().getNombre());
		data.addAliases("madre.hijos", personaWithSons.getMadre().getHijos());

		Persona result = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertNotNull(result);
		assertNotNull(result.getNombre());
		assertNotNull(result.getMadre().getHijos());
		assertNotNull(result.getMadre().getNombre());
	}

	@Test
	public void transforms_withProvidedRootBean() {
		Persona rootObject = new Persona();
		rootObject.setNombre("rootName");
		nestedTransformer = new AliastToTrimNestedResultTransformer(rootObject, Persona.class);

		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("madre.nombre", "madre");
		data.addAliases("madre.edad", Integer.valueOf(89));
		data.addAliases("padre.nombre", null);

		Persona resultNested = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertEquals("rootName", resultNested.getNombre());
		assertTrue(resultNested == rootObject);

		assertNotNull(resultNested.getMadre());
		assertEquals("madre", resultNested.getMadre().getNombre());
		assertEquals(Integer.valueOf(89), resultNested.getMadre().getEdad());
		assertNull(resultNested.getPadre());

	}

	@Test
	public void transforms_withProvidedRootBeanAndData() {
		Persona rootObject = new Persona();
		rootObject.setNombre("rootName");

		rootObject.setMadre(new Persona());
		rootObject.getMadre().setNombre("originalMadre");
		rootObject.getMadre().setMadre(new Persona());
		rootObject.getMadre().getMadre().setNombre("originalMadreMadre");
		nestedTransformer = new AliastToTrimNestedResultTransformer(rootObject, Persona.class);

		MockDataBuilder data = new MockDataBuilder();
		data.addAliases("madre.edad", Integer.valueOf(30));
		data.addAliases("madre.madre.edad", Integer.valueOf(60));
		data.addAliases("padre.nombre", null);

		Persona resultNested = (Persona) nestedTransformer.transformTuple(data.getTuple(), data.getAliases());

		assertTrue(resultNested == rootObject);
		assertEquals("rootName", resultNested.getNombre());

		assertNotNull(resultNested.getMadre());
		assertEquals("originalMadre", resultNested.getMadre().getNombre());
		assertEquals(Integer.valueOf(30), resultNested.getMadre().getEdad());
		assertEquals("originalMadreMadre", resultNested.getMadre().getMadre().getNombre());
		assertNull(resultNested.getPadre());

	}

}
