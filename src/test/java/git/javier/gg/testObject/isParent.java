package git.javier.gg.testObject;

import java.util.List;

/**
 * The contract for who is a parent
 * 
 * @author Javier Gonzalez
 * @version 1.0
 * @since 30 jul. 2020
 */
public interface isParent {

	List<Persona> getHijos();

	void setHijos(List<Persona> hijos);

}