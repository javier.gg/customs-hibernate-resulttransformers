package git.javier.gg.testObject;

/**
 * A contract for who is a son
 * 
 * @author Javier Gonzalez
 * @version 1.0
 * @since 30 jul. 2020
 */
public interface isSon {

	isSon getPadre();

	isSon getMadre();

}