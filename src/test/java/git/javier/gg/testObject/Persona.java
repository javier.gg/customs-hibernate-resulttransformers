package git.javier.gg.testObject;

import java.io.Serializable;
import java.util.List;

/**
 * A class that represent a people
 * 
 * @author Javier Gonzalez
 * @version 1.0
 * @since 30 jul. 2020
 */
public class Persona implements Serializable, isParent, isSon {

	private static final long serialVersionUID = 8597188987673359460L;

	private String nombre;

	private Integer edad;

	private Double estatura;

	private Persona padre;

	private Persona madre;

	private List<Persona> hijos;

	public Persona() {
		super();
	}

	public void setPadre(Persona padre) {
		this.padre = padre;
	}

	public void setMadre(Persona madre) {
		this.madre = madre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Double getEstatura() {
		return estatura;
	}

	public void setEstatura(Double estatura) {
		this.estatura = estatura;
	}

	public List<Persona> getHijos() {
		return hijos;
	}

	public void setHijos(List<Persona> hijos) {
		this.hijos = hijos;
	}

	public Persona getPadre() {
		return padre;
	}

	public Persona getMadre() {
		return madre;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null && !(obj instanceof Persona)) {
			return false;
		}

		Persona persona = (Persona) obj;

		if ((this.nombre == (persona.nombre) || this.nombre.equals(persona.nombre))
				&& (this.edad == (persona.edad) || this.edad.equals(persona.edad))
				&& (this.estatura == (persona.estatura) || this.estatura.equals(persona.estatura))
				&& (this.padre == (persona.padre) || this.padre.equals(persona.padre))
				&& (this.madre == (persona.madre) || this.madre.equals(persona.madre))
				&& (this.hijos == (persona.hijos) || this.hijos.equals(persona.hijos))) {
			return true;
		}

		return false;

	}
}