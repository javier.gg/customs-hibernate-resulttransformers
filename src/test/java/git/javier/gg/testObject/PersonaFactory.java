package git.javier.gg.testObject;

import java.util.ArrayList;

/**
 * a factory that provide nested entities of typ {@link Persona}
 * 
 * @author Javier Gonzalez
 * @version 1.0
 * @since 30 jul. 2020
 */
public class PersonaFactory {

	public static isParent getPersonShallow() {
		Persona persona = new Persona();
		persona.setNombre("Raul juarez perez");
		persona.setEdad(12);
		persona.setEstatura(1.67);
		persona.setPadre(null);
		persona.setMadre(null);
		persona.setHijos(null);
		return persona;
	}

	public static Persona getPersonaWithParentsShallow() {
		Persona padreShallow = getPadreShallow();
		Persona madreShallow = getMadreShallow();

		Persona hijo = new Persona();
		hijo.setNombre("Raul juarez perez");
		hijo.setEdad(24);
		hijo.setEstatura(1.67);
		hijo.setPadre(padreShallow);
		hijo.setMadre(madreShallow);
		hijo.setHijos(null);

		ArrayList<Persona> hijos = new ArrayList<Persona>();
		hijos.add(hijo);
		madreShallow.setHijos(hijos);
		padreShallow.setHijos(hijos);

		return hijo;
	}

	private static Persona getMadreShallow() {
		Persona madre = new Persona();
		madre.setNombre("Maria Perez perez");
		madre.setEdad(45);
		madre.setEstatura(1.63);
		return madre;
	}

	private static Persona getPadreShallow() {
		Persona padre = new Persona();
		padre.setNombre("Jesus juarez juarez");
		padre.setEdad(47);
		padre.setEstatura(1.60);
		return padre;
	}

	public static Persona getPersonaWithGrandParentsShallow() {
		Persona padreShallow = getPadreWithParentsShallow();
		Persona madreShallow = getMadreWithParentsShallow();

		Persona hijo = new Persona();
		hijo.setNombre("Polo Perez perez");
		hijo.setEdad(23);
		hijo.setEstatura(1.66);
		hijo.setPadre(padreShallow);
		hijo.setMadre(madreShallow);
		hijo.setHijos(null);

		ArrayList<Persona> hijos = new ArrayList<Persona>();
		hijos.add(hijo);
		madreShallow.setHijos(hijos);
		padreShallow.setHijos(hijos);

		return hijo;
	}

	private static Persona getMadreWithParentsShallow() {
		Persona abueloShallow = getAbueloMaternaShallow();
		Persona abuelaShallow = getAbuelaMaternaShallow();

		Persona madre = new Persona();
		madre.setNombre("Marta Ramirez Ramirez");
		madre.setEdad(32);
		madre.setEstatura(1.58);
		madre.setPadre(abueloShallow);
		madre.setMadre(abuelaShallow);
		madre.setHijos(null);

		ArrayList<Persona> hijos = new ArrayList<Persona>();
		hijos.add(madre);
		abuelaShallow.setHijos(hijos);
		abueloShallow.setHijos(hijos);
		return madre;
	}

	private static Persona getAbuelaMaternaShallow() {
		Persona abuela = new Persona();
		abuela.setNombre("Genobeba Ramirez Fariaz");
		abuela.setEdad(80);
		abuela.setEstatura(1.60);
		abuela.setPadre(null);
		abuela.setMadre(null);
		abuela.setHijos(null);
		return abuela;
	}

	private static Persona getAbueloMaternaShallow() {
		Persona abuela = new Persona();
		abuela.setNombre("Toribio Ramirez Paz");
		abuela.setEdad(91);
		abuela.setEstatura(1.65);
		abuela.setPadre(null);
		abuela.setMadre(null);
		abuela.setHijos(null);
		return abuela;
	}

	private static Persona getPadreWithParentsShallow() {
		Persona abueloShallow = getAbuelaPaternoShallow();
		Persona abuelaShallow = getAbueloPaternoShallow();

		Persona padre = new Persona();
		padre.setNombre("Roberto Romero De leon");
		padre.setEdad(40);
		padre.setEstatura(1.70);
		padre.setMadre(abuelaShallow);
		padre.setPadre(abueloShallow);

		ArrayList<Persona> hijos = new ArrayList<Persona>();
		hijos.add(padre);
		abuelaShallow.setHijos(hijos);
		abueloShallow.setHijos(hijos);
		return padre;
	}

	private static Persona getAbuelaPaternoShallow() {
		Persona abuela = new Persona();
		abuela.setNombre("Patricia Romero Fariaz");
		abuela.setEdad(93);
		abuela.setEstatura(1.72);
		abuela.setPadre(null);
		abuela.setMadre(null);
		abuela.setHijos(null);
		return abuela;
	}

	private static Persona getAbueloPaternoShallow() {
		Persona abuela = new Persona();
		abuela.setNombre("Tito De Leon Paz");
		abuela.setEdad(90);
		abuela.setEstatura(1.6);
		abuela.setPadre(null);
		abuela.setMadre(null);
		abuela.setHijos(null);
		return abuela;
	}
}
