package git.javier.gg.CustomResultTransformers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map.Entry;

import org.hibernate.HibernateException;
import org.hibernate.property.ChainedPropertyAccessor;
import org.hibernate.property.Getter;
import org.hibernate.property.PropertyAccessor;
import org.hibernate.property.PropertyAccessorFactory;
import org.hibernate.property.Setter;

public class BaseResultTranformerImpl {

	protected PropertyAccessor propertyAccessor;
	protected Class<?> rootResultClass;
	protected Object rootBeanObject;

	public BaseResultTranformerImpl(Object rootObject, Class<?> rootClass) {
		this.rootBeanObject = rootObject;
		this.rootResultClass = rootClass;
	}

	public BaseResultTranformerImpl(Class<?> rootClass) {
		this.rootBeanObject = null;
		this.rootResultClass = rootClass;
	}

	protected HibernateException createInstantiateHibernateException(Exception e, String className) {
		return new HibernateException("Could not instantiate bean: " + className, e);
	}

	protected void initPropertyAccessor() {
		if (propertyAccessor == null) {

			propertyAccessor = new ChainedPropertyAccessor(
					new PropertyAccessor[] { PropertyAccessorFactory.getPropertyAccessor("property"),
							PropertyAccessorFactory.getPropertyAccessor("field") });
		}
	}

	protected void initResultBean() {
		if (rootBeanObject == null) {
			rootBeanObject = createBean(rootResultClass);
		}
	}

	protected Object createBean(Class<?> sourceClass) {
		try {
			Constructor<?> constructor = sourceClass.getConstructor();
			return constructor.newInstance();
		} catch (NoSuchMethodException e) {
			throw createInstantiateHibernateException(e, sourceClass.getName());
		} catch (SecurityException e) {
			throw createInstantiateHibernateException(e, sourceClass.getName());
		} catch (InstantiationException e) {
			throw createInstantiateHibernateException(e, sourceClass.getName());
		} catch (IllegalAccessException e) {
			throw createInstantiateHibernateException(e, sourceClass.getName());
		} catch (IllegalArgumentException e) {
			throw createInstantiateHibernateException(e, sourceClass.getName());
		} catch (InvocationTargetException e) {
			throw createInstantiateHibernateException(e, sourceClass.getName());
		}
	}

	protected void processNestedAliases(Object rootResult, NestedAliasesContainer containerNested) {
		for (Entry<String, TupleAlias> nestedToProcess : containerNested.getOrderedNestedAliases()) {
			String rootNestedAlias = nestedToProcess.getKey();
			TupleAlias nestedTupleAliasOnNestedRoot = nestedToProcess.getValue();
			String[] aliasAsArray = nestedTupleAliasOnNestedRoot.getAliasAsArray();
			Object[] tupleAsArray = nestedTupleAliasOnNestedRoot.getTupleAsArray();

			Getter getter = propertyAccessor.getGetter(rootResultClass, rootNestedAlias);
			Class<?> nestedClass = getter.getReturnType();
			Object nestedBean = getter.get(rootResult);
			if (nestedBean == null) {
				nestedBean = createBean(nestedClass);
			}

			AliastToFullNestedResultTransformer nestedTranformer = new AliastToFullNestedResultTransformer(nestedBean,
					nestedClass);
			Object nestedResult = nestedTranformer.transformTuple(tupleAsArray, aliasAsArray);

			Setter setter = propertyAccessor.getSetter(rootResultClass, rootNestedAlias);
			if (setter != null) {
				setter.set(rootResult, nestedResult, null);
			}
		}

	}

}
