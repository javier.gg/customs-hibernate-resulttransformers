package git.javier.gg.CustomResultTransformers;

import java.util.ArrayList;

public class TupleAlias {

	private ArrayList<String> alias = new ArrayList<String>();
	private ArrayList<Object> tuple = new ArrayList<Object>();

	public ArrayList<Object> getTuple() {
		return tuple;
	}

	public Object[] getTupleAsArray() {
		return tuple.toArray(new Object[tuple.size()]);
	}

	public void setTuple(ArrayList<Object> tuple) {
		this.tuple = tuple;
	}

	public ArrayList<String> getAlias() {
		return alias;
	}

	public String[] getAliasAsArray() {
		return alias.toArray(new String[alias.size()]);
	}

	public void setAlias(ArrayList<String> alias) {
		this.alias = alias;
	}

}
