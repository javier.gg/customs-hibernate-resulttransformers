package git.javier.gg.CustomResultTransformers;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class NestedAliasesContainer {

	private HashMap<String, TupleAlias> childAliases = new HashMap<String, TupleAlias>();

	/**
	 * @param aliasWithNestedTree to add
	 * @param resultValue         respectively to the aliases
	 */
	public void addNestedAlias(String aliasWithNestedTree, Object resultValue) {
		String[] split = aliasWithNestedTree.split("\\.", 2);
		String rootKeyAlias = split[0];
		String nestedAlias = split[1];

		if (!childAliases.containsKey(rootKeyAlias)) {
			TupleAlias nestedTupleAlias = new TupleAlias();
			nestedTupleAlias.getAlias().add(nestedAlias);
			nestedTupleAlias.getTuple().add(resultValue);

			childAliases.put(rootKeyAlias, nestedTupleAlias);
		} else {
			TupleAlias nestedTupleAlias = childAliases.get(rootKeyAlias);
			nestedTupleAlias.getAlias().add(nestedAlias);
			nestedTupleAlias.getTuple().add(resultValue);
		}

	}

	/**
	 * Get the contents as a entries, each which has as key the rootAlias and as
	 * value the nested tupleAlias.
	 * 
	 * @return a set with the entries
	 */
	public Set<Entry<String, TupleAlias>> getOrderedNestedAliases() {
		return childAliases.entrySet();
	}
}
