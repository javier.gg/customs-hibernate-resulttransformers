package git.javier.gg.CustomResultTransformers;

import java.util.List;

import org.hibernate.property.Setter;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.ResultTransformer;

/**
 * A implementation that mimics the {@link AliasToBeanResultTransformer} with
 * support for multi-nested properties.<br>
 * 
 * @author Javier Gonzalez
 * @version 1.0
 * @since 25 jul. 2020
 */
public class AliastToTrimNestedResultTransformer extends BaseResultTranformerImpl implements ResultTransformer {

	private static final long serialVersionUID = 1239832L;

	public AliastToTrimNestedResultTransformer(Class<?> rootClass) {
		super(rootClass);
	}

	public AliastToTrimNestedResultTransformer(Object rootObject, Class<?> rootClass) {
		super(rootObject, rootClass);
	}

	@SuppressWarnings("rawtypes")
	public List transformList(List collection) {
		return collection;
	}

	public Object transformTuple(Object[] tuple, String[] aliases) {

		try {
			initResultBean();
			initPropertyAccessor();

			NestedAliasesContainer containerOfNested = new NestedAliasesContainer();
			for (int i = 0; i < aliases.length; i++) {
				String alias = aliases[i];
				Object value = tuple[i];
				if (alias == null || value == null) {
					continue;
				}

				if (alias.contains(".")) {
					containerOfNested.addNestedAlias(alias, value);

				} else if (value != null) {

					Setter setter = propertyAccessor.getSetter(rootResultClass, alias);
					if (setter != null) {
						setter.set(rootBeanObject, value, null);
					}
				}
			}

			processNestedAliases(rootBeanObject, containerOfNested);

		} catch (SecurityException e) {
			throw createInstantiateHibernateException(e, rootResultClass.getName());
		} catch (IllegalArgumentException e) {
			throw createInstantiateHibernateException(e, rootResultClass.getName());
		}

		return rootBeanObject;
	}

}
