 Customs Results Transformers
==================================================

## Goals
Provide ResultTransformers implementations that have case uses which are frequently raised.

## Example of use
Use of one resultTransformer on the criteria builder.

## Description 

There are multiple resultTransformers variants(TrimTree, FullTree, ProvidedRoot), each one have theirs owns case uses. 

#### TrimTree
Build the tree of objects ignoring the paths that has a result-value of NULL.
**Case uses**
- You not desires that extra spend of time on build path that will save a NULL
- You will treat the bean in a form on which the missing paths are not a big deal
- You desires indicate that a missing path is a missing value

#### FullTree
Build the full tree of objects, as specify on the aliases, either the result is a value or a NULL.
**Case uses**
- You not desieres to have checks for every nested path because feared of a NULL pointerException
- The missing result-values could be set after all to a default value

#### Provided Root (Variant):
Use a already created bean for fill the gaps. Useful when you need fill a bean from different resources,e.g., DB, FileSystem, computed...
**Case uses**
- You already have a Bean with some information, and need complement information with a DB query.
- You need fill a bean with two DB querys, each one is from different BDs

## Ejemplos detallados 


## Autores
 javierGG

## Licencia
This project is licensed under the GNU LGP License - see the LICENSE file for details

## Version history
- **0.0.1** 
   - initial release